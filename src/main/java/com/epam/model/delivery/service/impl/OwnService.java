package com.epam.model.delivery.service.impl;

import com.epam.controller.factory.delivery.PizzaDeliveryFactory;
import com.epam.model.delivery.service.DeliveryService;
import com.epam.model.enums.DeliveryType;
import com.epam.model.pizza.Pizza;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OwnService extends DeliveryService {

    public OwnService(PizzaDeliveryFactory pizzaDeliveryFactory) {
        this.pizzaDeliveryFactory = pizzaDeliveryFactory;
        this.serviceType = DeliveryType.DELIVEROO;
    }
}
