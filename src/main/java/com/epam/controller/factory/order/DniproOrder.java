package com.epam.controller.factory.order;

import com.epam.model.Order;
import com.epam.model.enums.City;
import com.epam.model.enums.DeliveryType;
import com.epam.model.enums.PizzaType;
import com.epam.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class DniproOrder {
    private static Logger log = LogManager.getLogger(DniproOrder.class);
    private static Scanner input = new Scanner(System.in);
    Order dniproOrder;

    private void getCheesePizza() {
        dniproOrder.addPizzaToOrder(PizzaType.CHEESE);
    }

    private void getCapricciosa() {
        dniproOrder.addPizzaToOrder(PizzaType.CAPRICCIOSA);
    }

    private void getPepperoniPizza() {
        dniproOrder.addPizzaToOrder(PizzaType.PEPPERONI);
    }

    private void getVeggie() {
        dniproOrder.addPizzaToOrder(PizzaType.VEGGIE);
    }

    public void showDniproOrder() {
        String keyMenu;
        dniproOrder = new Order(City.DNIPRO);
        do {
            Menu.getPizzaMenu();
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        getCheesePizza();
                        break;
                    case "2":
                        getCapricciosa();
                        break;
                    case "3":
                        getPepperoniPizza();
                        break;
                    case "4":
                        getVeggie();
                        break;
                    case "Q":
                        log.info("Approved for delivery status: \n");
                        log.info(dniproOrder.checkPizzaReadyToDeliver());
                        dniproDeliver();
                        break;
                }
            } catch (Exception e) {

            }
        } while (!keyMenu.equals("Q"));
    }

    private void dniproDeliver() {

        dniproOrder.deliverOrderToClient(DeliveryType.UBER, 50);
        String keyMenu;
        do {
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        getCheesePizza();
                        break;
                    case "2":
                        getCapricciosa();
                        break;
                    case "3":
                        getPepperoniPizza();
                        break;
                    case "4":
                        getVeggie();
                        break;
                    case "Q":
                        log.info("Approved for delivery status: \n");
                        log.info(dniproOrder.checkPizzaReadyToDeliver());
                        Menu.getDeliveryMenu();
                        dniproOrder.deliverOrderToClient(DeliveryType.UBER, 50);
                        break;
                }
            } catch (Exception e) {

            }
        } while (!keyMenu.equals("Q"));
    }
}
