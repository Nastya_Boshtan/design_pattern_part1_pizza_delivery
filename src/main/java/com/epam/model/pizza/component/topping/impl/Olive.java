package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Olive implements Topping {
    public String toString() {
        return "Olive";
    }

    @Override
    public double getPrice() {
        return 11;
    }
}
