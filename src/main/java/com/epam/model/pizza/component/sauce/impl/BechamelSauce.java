package com.epam.model.pizza.component.sauce.impl;

import com.epam.model.pizza.component.sauce.Sauce;

public class BechamelSauce implements Sauce {
    public String toString() {
        return "Bechamel";
    }

    @Override
    public double getPrice() {
        return 20;
    }
}
