package com.epam.model.enums;

public enum PizzaType {
    CAPRICCIOSA, CHEESE, PEPPERONI, VEGGIE
}
