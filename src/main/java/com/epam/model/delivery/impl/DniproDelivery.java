package com.epam.model.delivery.impl;

import com.epam.controller.factory.delivery.PizzaDeliveryFactory;
import com.epam.controller.factory.delivery.impl.KyivPizzaDeliveryFactory;
import com.epam.model.delivery.Delivery;
import com.epam.model.delivery.service.DeliveryService;
import com.epam.model.delivery.service.impl.GlovoService;
import com.epam.model.delivery.service.impl.OwnService;
import com.epam.model.delivery.service.impl.UberService;
import com.epam.model.enums.DeliveryType;
import com.epam.model.pizza.Pizza;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DniproDelivery extends Delivery {

  private static Logger log = LogManager.getLogger(DniproDelivery.class);

  @Override
  protected double deliverPizza(DeliveryType deliveryType, List<Pizza> pizzas, int distance) {
    PizzaDeliveryFactory factory = new KyivPizzaDeliveryFactory();
    double price;
    DeliveryService deliveryService;
    switch (deliveryType) {
      case UBER:
        deliveryService = new UberService(factory);
        price = deliveryService.deliver(pizzas, distance);
        break;
      case GLOVO:
        deliveryService = new GlovoService(factory);
        price = deliveryService.deliver(pizzas, distance);
        break;
      case DELIVEROO:
        deliveryService = new OwnService(factory);
        price = deliveryService.deliver(pizzas, distance);
        break;
      default:
        price = 0D;
    }
    return price;
  }
}
