package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Prosciutto implements Topping {
    public String toString() {
        return "Prosciutto";
    }

    @Override
    public double getPrice() {
        return 7;
    }
}
