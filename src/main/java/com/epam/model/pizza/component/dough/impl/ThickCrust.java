package com.epam.model.pizza.component.dough.impl;

import com.epam.model.pizza.component.dough.Dough;

public class ThickCrust implements Dough {
    public String toString() {
        return "Thick crust";
    }

    @Override
    public double getPrice() {
        return 30;
    }
}
