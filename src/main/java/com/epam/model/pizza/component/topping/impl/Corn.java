package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Corn implements Topping {
    public String toString() {
        return "Corn";
    }

    @Override
    public double getPrice() {
        return 8;
    }
}
