package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Ham implements Topping {
    public String toString() {
        return "Ham";
    }

    @Override
    public double getPrice() {
        return 13;
    }
}
