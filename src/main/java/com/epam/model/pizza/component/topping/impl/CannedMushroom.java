package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class CannedMushroom implements Topping {
    public String toString() {
        return "Canned Mushrooms";
    }

    @Override
    public double getPrice() {
        return 14;
    }
}
