package com.epam.model.pizza;

import com.epam.model.enums.PizzaStatus;
import com.epam.model.enums.PizzaType;
import com.epam.model.pizza.component.dough.Dough;
import com.epam.model.pizza.component.sauce.Sauce;
import com.epam.model.pizza.component.topping.Topping;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Pizza {

  private static Logger log = LogManager.getLogger(Pizza.class);
  protected PizzaType pizzaType;
  protected Dough dough;
  protected Sauce sauce;
  protected PizzaStatus pizzaStatus;
  protected double price;

  protected List<Topping> toppings = new ArrayList<>();

  public abstract void prepare();

  public void bake() {
    log.info("Bake pizza. Remove the pizza after 30 minutes");
    pizzaStatus = PizzaStatus.BAKED;
  }

  public void cut() {
    log.info("Cutting pizza...");
    pizzaStatus = PizzaStatus.CUTTED;
  }

  public void box() {
    log.info("Packing pizza for delivery...");
    pizzaStatus = PizzaStatus.BOXED;
  }

  public int getPrice() {
    int price = 0;
    for (Topping topping : toppings) {
      price += topping.getPrice();
    }
    return price;
  }

  @Override
  public String toString() {

    StringBuffer toppingString = new StringBuffer();

        return "{" +
            "pizzaType=" + pizzaType +
            ", dough=" + dough +
            ", sauce=" + sauce +
            ", toppings=" + toppingString.toString() +
            ", price="+price+
            '}';
    }

    public PizzaStatus getPizzaStatus() {
        return pizzaStatus;
    }

    public void setPizzaStatus(PizzaStatus pizzaStatus) {
        this.pizzaStatus = pizzaStatus;
    }
}
