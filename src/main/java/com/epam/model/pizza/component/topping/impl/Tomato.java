package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Tomato implements Topping {
    public String toString() {
        return "Tomato";
    }

    @Override
    public double getPrice() {
        return 15;
    }
}
