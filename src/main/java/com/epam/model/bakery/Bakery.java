package com.epam.model.bakery;

import com.epam.model.pizza.Pizza;
import com.epam.model.enums.PizzaType;

public abstract class Bakery {

  protected abstract Pizza getPizza(PizzaType pizzaType);

  public Pizza orderPizza(PizzaType pizzaType) {
    Pizza pizza = getPizza(pizzaType);
    if (pizza != null) {
      pizza.prepare();
      pizza.bake();
      pizza.cut();
      pizza.box();
    }
    return pizza;
  }
}
