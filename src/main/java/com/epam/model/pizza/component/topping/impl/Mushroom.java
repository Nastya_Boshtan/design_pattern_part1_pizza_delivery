package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Mushroom implements Topping {
    public String toString() {
        return "Mushrooms";
    }

    @Override
    public double getPrice() {
        return 15;
    }
}
