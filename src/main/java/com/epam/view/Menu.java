package com.epam.view;

import com.epam.controller.factory.order.DniproOrder;
import com.epam.controller.factory.order.KyivOrder;
import com.epam.controller.factory.order.LvivOrder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    static Map<String, String> pizzaMenu;
    static Map<String, String> deliveryService;
    private static Logger log = LogManager.getLogger(Menu.class);
    private static Scanner input = new Scanner(System.in);
    LvivOrder lvivOrder = new LvivOrder();
    KyivOrder kyivOrder = new KyivOrder();
    DniproOrder dniproOrder = new DniproOrder();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Map<String, String> cityMenu;

    public Menu() {
        cityMenu = new LinkedHashMap<>();
        cityMenu.put("1", "1 - Lviv");
        cityMenu.put("2", "2 - Dnipro");
        cityMenu.put("3", "3 - Kyiv");
        cityMenu.put("Q", "Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    public static void getPizzaMenu() {
        log.info("\n Please, choose a pizza from the list: ");
        pizzaMenu = new LinkedHashMap<>();
        pizzaMenu.put("1", " 1 - Cheese Pizza");
        pizzaMenu.put("2", " 2 - Capricciosa Pizza");
        pizzaMenu.put("3", " 3 - Pepperoni Pizza");
        pizzaMenu.put("4", " 4 - Veggie Pizza");
        pizzaMenu.put("Q", " Q - exit");

        pizzaMenu.forEach((K, V) -> log.info(V));
    }

    public static void getDeliveryMenu() {
        log.info("\n Please, choose a delivery service from the list: ");
        deliveryService = new LinkedHashMap<>();
        deliveryService.put("1", " 1 - Uber");
        deliveryService.put("2", " 2 - Glovo");
        deliveryService.put("3", " 3 - Own");
        deliveryService.put("Q", " Q - exit");

        deliveryService.forEach((K, V) -> log.info(V));
    }

    private void pressButton1() {
        lvivOrder.showLvivOrder();
    }

    private void pressButton2() {
        dniproOrder.showDniproOrder();
    }

    private void pressButton3() {
        kyivOrder.showKyivOrder();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : cityMenu.values()) {
            log.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            log.info("Please, choose the city");
            outputMenu();
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

