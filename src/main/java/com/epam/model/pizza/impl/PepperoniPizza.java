package com.epam.model.pizza.impl;

import com.epam.controller.factory.prepare.PizzaPrepareFactory;
import com.epam.model.enums.PizzaStatus;
import com.epam.model.enums.PizzaType;
import com.epam.model.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PepperoniPizza extends Pizza {

  private static Logger log = LogManager.getLogger(PepperoniPizza.class);
  private PizzaPrepareFactory pizzaPrepareFactory;

  public PepperoniPizza(PizzaPrepareFactory pizzaPrepareFactory) {
    this.pizzaStatus = PizzaStatus.ORDERED;
    this.pizzaPrepareFactory = pizzaPrepareFactory;
    this.pizzaType = PizzaType.PEPPERONI;
  }

  public void prepare() {
    log.info("Started preparing pizza " + this.pizzaType);
    this.dough = pizzaPrepareFactory.getDough();
    this.sauce = pizzaPrepareFactory.getSauce();
    this.toppings.add(pizzaPrepareFactory.getOlive());
    this.toppings.add(pizzaPrepareFactory.getMushroom());
    this.toppings.add(pizzaPrepareFactory.getCorn());
    this.toppings.add(pizzaPrepareFactory.getMeat());
    this.toppings.add(pizzaPrepareFactory.getCheese());
    this.price =
        pizzaPrepareFactory.getDough().getPrice() + pizzaPrepareFactory.getSauce().getPrice()
            + getPrice();
    log.info(String.format("Pizza %s already prepared", this));
    this.pizzaStatus = PizzaStatus.PREPARED;
  }
}
