package com.epam.controller.factory.order;

import com.epam.model.Order;
import com.epam.model.enums.City;
import com.epam.model.enums.DeliveryType;
import com.epam.model.enums.PizzaType;
import com.epam.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class LvivOrder {
    private static Logger log = LogManager.getLogger(LvivOrder.class);
    private static Scanner input = new Scanner(System.in);
    Order lvivOrder;

    private void getCheesePizza() {
        lvivOrder.addPizzaToOrder(PizzaType.CHEESE);
    }

    private void getCapricciosa() {
        lvivOrder.addPizzaToOrder(PizzaType.CAPRICCIOSA);
    }

    private void getPepperoniPizza() {
        lvivOrder.addPizzaToOrder(PizzaType.PEPPERONI);
    }

    private void getVeggie() {
        lvivOrder.addPizzaToOrder(PizzaType.VEGGIE);
    }

    public void showLvivOrder() {
        String keyMenu;
        lvivOrder = new Order(City.LVIV);
        do {
            Menu.getPizzaMenu();
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        getCheesePizza();
                        break;
                    case "2":
                        getCapricciosa();
                        break;
                    case "3":
                        getPepperoniPizza();
                        break;
                    case "4":
                        getVeggie();
                    case "Q":
                        log.info("Approved for delivery status: \n");
                        log.info(lvivOrder.checkPizzaReadyToDeliver());
                        Menu.getDeliveryMenu();
                        lvivOrder.deliverOrderToClient(DeliveryType.UBER, 50);
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}

