package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class CottageCheese implements Topping {
    public String toString() {
        return "Cottage cheese";
    }

    @Override
    public double getPrice() {
        return 17;
    }
}
