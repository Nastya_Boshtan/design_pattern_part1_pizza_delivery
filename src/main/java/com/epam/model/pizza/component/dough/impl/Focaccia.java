package com.epam.model.pizza.component.dough.impl;

import com.epam.model.pizza.component.dough.Dough;

public class Focaccia implements Dough {
    public String toString() {
        return "Focaccia";
    }

    @Override
    public double getPrice() {
        return 10;
    }
}
