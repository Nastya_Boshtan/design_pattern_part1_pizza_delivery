package com.epam.model.pizza.impl;

import com.epam.controller.factory.prepare.PizzaPrepareFactory;
import com.epam.model.enums.PizzaStatus;
import com.epam.model.enums.PizzaType;
import com.epam.model.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class CapricciosaPizza extends Pizza {

  private static Logger log = LogManager.getLogger(CapricciosaPizza.class);
  private PizzaPrepareFactory pizzaPrepareFactory;

  public CapricciosaPizza(PizzaPrepareFactory pizzaPrepareFactory) {
    this.pizzaStatus = PizzaStatus.ORDERED;
    this.pizzaPrepareFactory = pizzaPrepareFactory;
    this.pizzaType = PizzaType.CAPRICCIOSA;
  }

  public void prepare() {
    log.info("Started preparing pizza " + this.pizzaType);
    this.dough = pizzaPrepareFactory.getDough();
    this.sauce = pizzaPrepareFactory.getSauce();
    this.toppings.add(pizzaPrepareFactory.getPineapple());
    this.toppings.add(pizzaPrepareFactory.getOlive());
    this.toppings.add(pizzaPrepareFactory.getBacon());
    this.toppings.add(pizzaPrepareFactory.getMeat());
    this.price =
        pizzaPrepareFactory.getDough().getPrice() + pizzaPrepareFactory.getSauce().getPrice()
            + getPrice();
    log.info(String.format("Pizza %s already prepared", this));
    this.pizzaStatus = PizzaStatus.PREPARED;
  }

}
