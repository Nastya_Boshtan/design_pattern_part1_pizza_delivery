package com.epam.model.delivery.service;

import com.epam.controller.factory.delivery.PizzaDeliveryFactory;
import com.epam.model.enums.DeliveryType;
import com.epam.model.pizza.Pizza;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class DeliveryService {

  private static Logger log = LogManager.getLogger(DeliveryService.class);

  protected PizzaDeliveryFactory pizzaDeliveryFactory;
  protected DeliveryType serviceType;

  protected double price;
  protected String phoneNumber;

  public double getPrice() {
    return price;
  }

  public double deliver(List<Pizza> pizzas, int distance) {
    log.info("Delivery by " + serviceType);
    double pizzaPrices = 0D;
    for (Pizza pizza : pizzas) {
      if (pizza != null) {
        pizzaPrices += pizza.getPrice();
      }
    }
    price = pizzaDeliveryFactory.getPrice(distance, pizzaPrices);
    phoneNumber = pizzaDeliveryFactory.getPhone();
    return price;
  }

  public void orderService() {
    System.out.println("Order implementation...");
  }

  public void pay(double price) {
    System.out.println("Price to pay: " + price);
  }
}
