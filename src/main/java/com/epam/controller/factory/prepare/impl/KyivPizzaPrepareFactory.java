package com.epam.controller.factory.prepare.impl;

import com.epam.controller.factory.prepare.PizzaPrepareFactory;
import com.epam.model.pizza.component.dough.Dough;
import com.epam.model.pizza.component.sauce.Sauce;
import com.epam.model.pizza.component.topping.Topping;
import com.epam.model.pizza.component.dough.impl.ThickCrust;
import com.epam.model.pizza.component.sauce.impl.MarinaraSauce;
import com.epam.model.pizza.component.topping.impl.Bacon;
import com.epam.model.pizza.component.topping.impl.CannedMushroom;
import com.epam.model.pizza.component.topping.impl.Chicken;
import com.epam.model.pizza.component.topping.impl.Corn;
import com.epam.model.pizza.component.topping.impl.CottageCheese;
import com.epam.model.pizza.component.topping.impl.Olive;
import com.epam.model.pizza.component.topping.impl.Onion;
import com.epam.model.pizza.component.topping.impl.Pineapple;
import com.epam.model.pizza.component.topping.impl.Spinach;
import com.epam.model.pizza.component.topping.impl.Tomato;

public class KyivPizzaPrepareFactory implements PizzaPrepareFactory {


  @Override
  public Dough getDough() {
    return new ThickCrust();
  }

  @Override
  public Sauce getSauce() {
    return new MarinaraSauce();
  }

  @Override
  public Topping getBacon() {
    return new Bacon();
  }

  @Override
  public Topping getOnion() {
    return new Onion();
  }

  @Override
  public Topping getMeat() {
    return new Chicken();
  }

  @Override
  public Topping getCorn() {
    return new Corn();
  }

  @Override
  public Topping getMushroom() {
    return new CannedMushroom();
  }

  @Override
  public Topping getCheese(){
    return new CottageCheese();
  }
  @Override
  public Topping getOlive() {
    return new Olive();
  }

  @Override
  public Topping getPineapple() {
    return new Pineapple();
  }

  @Override
  public Topping getSpinach() {
    return new Spinach();
  }

  @Override
  public Topping getTomato() {
    return new Tomato();
  }

}
