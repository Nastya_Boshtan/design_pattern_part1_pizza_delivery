package com.epam.controller.factory.delivery.impl;

import com.epam.controller.factory.delivery.PizzaDeliveryFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LvivPizzaDeliveryFactory implements PizzaDeliveryFactory {
    private static Logger log = LogManager.getLogger(LvivPizzaDeliveryFactory.class);
    private static final double lvivDeliveryFactor = 1.9;

    @Override
    public double getPrice(int distance, double pizzasPrice) {
        double price = distance * lvivDeliveryFactor;
        log.info(String.format("The cost of pizza delivery: " + price + ". Distance = " + distance + "km"));
        return price + pizzasPrice;
    }
    @Override
    public String getPhone(/*String phoneNumber*/) {
        log.info(String.format("Customer's phone number: "));
        return null;
    }

    @Override
    public int calculateDistance() {
        log.info("");
        return 0;
    }
}
