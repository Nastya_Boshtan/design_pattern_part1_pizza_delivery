package com.epam.model;

import com.epam.model.bakery.Bakery;
import com.epam.model.bakery.impl.DniproBakery;
import com.epam.model.bakery.impl.KyivBakery;
import com.epam.model.bakery.impl.LvivBakery;
import com.epam.model.delivery.Delivery;
import com.epam.model.delivery.impl.DniproDelivery;
import com.epam.model.delivery.impl.KyivDelivery;
import com.epam.model.delivery.impl.LvivDelivery;
import com.epam.model.enums.City;
import com.epam.model.enums.DeliveryType;
import com.epam.model.enums.PizzaStatus;
import com.epam.model.enums.PizzaType;
import com.epam.model.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private static Logger log = LogManager.getLogger(Order.class);
    //todo add orderStatus (get it from each of pizzas statuses)
    private Bakery pizzaBakery;
    private Delivery pizzaDelivery;
    private City city;

    private List<Pizza> pizzas = new ArrayList<>();

    public Order(City city) {
        log.info(String.format("Started ordering pizza in %s city", city));
        switch (city) {
            case LVIV:
                this.pizzaBakery = new LvivBakery();
                this.pizzaDelivery = new LvivDelivery();
                this.city = City.LVIV;
                break;
            case KYIV:
                this.pizzaBakery = new KyivBakery();
                this.pizzaDelivery = new KyivDelivery();
                this.city = City.KYIV;
                break;
            case DNIPRO:
                this.pizzaBakery = new DniproBakery();
                this.pizzaDelivery = new DniproDelivery();
                this.city = City.DNIPRO;
                break;
            default:
                log.info("We don't have bakery in this city");
        }
    }

    public void addPizzaToOrder(PizzaType pizzaType) {
        Pizza pizza = pizzaBakery.orderPizza(pizzaType);
        if (pizza != null) {
            pizzas.add(pizza);
        }
        log.info(String.format("Added to order pizza %s", pizzaType));
    }

    public void deliverOrderToClient(DeliveryType deliveryType, int distance) {
        if (checkPizzaReadyToDeliver()) {
            log.info("Order prepared. And ready to deliver");
            double price = pizzaDelivery.sendPizza(deliveryType, pizzas, distance);
            log.info(String.format("============Order price is %.2f", price));
        }
    }

    public boolean checkPizzaReadyToDeliver() {
        boolean done = false;
        for (Pizza pizza : pizzas) {
            if (pizza.getPizzaStatus() == PizzaStatus.BOXED) {
                done = true;
            } else {
                done = false;
            }
        }
        return done;
    }
}
