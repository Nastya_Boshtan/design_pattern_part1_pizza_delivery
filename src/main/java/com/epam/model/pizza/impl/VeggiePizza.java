package com.epam.model.pizza.impl;

import com.epam.controller.factory.prepare.PizzaPrepareFactory;
import com.epam.model.enums.PizzaStatus;
import com.epam.model.pizza.Pizza;
import com.epam.model.enums.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VeggiePizza extends Pizza {

  private static Logger log = LogManager.getLogger(VeggiePizza.class);
  private PizzaPrepareFactory pizzaPrepareFactory;

  public VeggiePizza(PizzaPrepareFactory pizzaPrepareFactory) {
    this.pizzaStatus = PizzaStatus.ORDERED;
    this.pizzaPrepareFactory = pizzaPrepareFactory;
    this.pizzaType = PizzaType.VEGGIE;
  }

  public void prepare() {
    log.info("Started preparing pizza " + this.pizzaType);
    this.dough = pizzaPrepareFactory.getDough();
    this.sauce = pizzaPrepareFactory.getSauce();
    this.toppings.add(pizzaPrepareFactory.getSpinach());
    this.toppings.add(pizzaPrepareFactory.getTomato());
    this.toppings.add(pizzaPrepareFactory.getOlive());
    this.toppings.add(pizzaPrepareFactory.getCorn());
    this.toppings.add(pizzaPrepareFactory.getMushroom());
    this.toppings.add(pizzaPrepareFactory.getOnion());
    this.price =
        pizzaPrepareFactory.getDough().getPrice() + pizzaPrepareFactory.getSauce().getPrice()
            + getPrice();
    log.info(String.format("Pizza %s already prepared", this));
    this.pizzaStatus = PizzaStatus.PREPARED;
  }
}
