package com.epam.model.pizza.component.sauce.impl;

import com.epam.model.pizza.component.sauce.Sauce;

public class BarbecueSauce implements Sauce {
    public String toString() {
        return "Barbecue";
    }

    @Override
    public double getPrice() {
        return 15;
    }
}
