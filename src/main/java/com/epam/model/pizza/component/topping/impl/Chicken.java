package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Chicken implements Topping {
    public String toString() {
        return "Chicken";
    }

    @Override
    public double getPrice() {
        return 17;
    }
}
