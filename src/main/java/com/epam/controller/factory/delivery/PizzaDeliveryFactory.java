package com.epam.controller.factory.delivery;

public interface PizzaDeliveryFactory {
    double getPrice(int distance, double pizzasPrice);

    String getPhone(/*String phoneNumber*/);

    int calculateDistance();
}
