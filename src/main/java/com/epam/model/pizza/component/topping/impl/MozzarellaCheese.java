package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class MozzarellaCheese implements Topping {
    public String toString() {
        return "Mozzarella";
    }

    @Override
    public double getPrice() {
        return 12;
    }
}
