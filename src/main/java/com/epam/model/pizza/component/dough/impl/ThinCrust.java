package com.epam.model.pizza.component.dough.impl;

import com.epam.model.pizza.component.dough.Dough;

public class ThinCrust implements Dough {
    public String toString() {
        return "Thin crust";
    }

    @Override
    public double getPrice() {
        return 25;
    }
}
