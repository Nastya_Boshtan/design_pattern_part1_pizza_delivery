package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Onion implements Topping {
    public String toString() {
        return "Red onion";
    }

    @Override
    public double getPrice() {
        return 12;
    }
}
