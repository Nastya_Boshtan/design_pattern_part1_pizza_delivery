package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Bacon implements Topping {
    public String toString() {
        return "Bacon";
    }

    @Override
    public double getPrice() {
        return 10;
    }
}
