package com.epam.model.enums;

public enum PizzaStatus {
  ORDERED, PREPARED, BAKED, DELIVERED, BOXED, CUTTED
}
