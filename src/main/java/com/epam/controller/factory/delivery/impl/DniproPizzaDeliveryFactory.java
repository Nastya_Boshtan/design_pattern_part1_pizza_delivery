package com.epam.controller.factory.delivery.impl;

import com.epam.controller.factory.delivery.PizzaDeliveryFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DniproPizzaDeliveryFactory implements PizzaDeliveryFactory {
    private static Logger log = LogManager.getLogger(DniproPizzaDeliveryFactory.class);
    private static final double dniproDeliveryFactor = 1.8;

    @Override
    public double getPrice(int distance, double pizzasPrice) {
        double price = distance * dniproDeliveryFactor;
        log.info(String.format("The cost of pizza delivery: " + price + ". Distance = " + distance + "km"));
        return price + pizzasPrice;
    }

    @Override
    public String getPhone(/*String phoneNumber*/) {
        return null;
    }

    @Override
    public int calculateDistance() {
        return 0;
    }
}
