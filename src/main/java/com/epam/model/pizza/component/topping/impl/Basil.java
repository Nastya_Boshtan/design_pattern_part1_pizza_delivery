package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Basil implements Topping {
    public String toString() {
        return "Basil";
    }

    @Override
    public double getPrice() {
        return 12;
    }
}
