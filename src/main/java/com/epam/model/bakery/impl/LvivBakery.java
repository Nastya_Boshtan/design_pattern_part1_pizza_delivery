package com.epam.model.bakery.impl;

import com.epam.controller.factory.prepare.PizzaPrepareFactory;
import com.epam.controller.factory.prepare.impl.LvivPizzaPrepareFactory;
import com.epam.model.pizza.Pizza;
import com.epam.model.bakery.Bakery;
import com.epam.model.enums.PizzaType;
import com.epam.model.pizza.impl.CapricciosaPizza;
import com.epam.model.pizza.impl.CheesePizza;
import com.epam.model.pizza.impl.PepperoniPizza;
import com.epam.model.pizza.impl.VeggiePizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LvivBakery extends Bakery {

  private static Logger log = LogManager.getLogger(LvivBakery.class);

  @Override
  protected Pizza getPizza(PizzaType pizzaType) {
    Pizza pizza;
    PizzaPrepareFactory factory = new LvivPizzaPrepareFactory();
    switch (pizzaType) {
      case CAPRICCIOSA:
        pizza = new CapricciosaPizza(factory);
        break;
      case CHEESE:
        pizza = new CheesePizza(factory);
        break;
      case VEGGIE:
        pizza = new VeggiePizza(factory);
        break;
      case PEPPERONI:
        pizza = new PepperoniPizza(factory);
        break;
      default:
        log.info(String.format("We don't have %s pizza type in this city", pizzaType));
        pizza = null;
    }
    return pizza;
  }
}
