package com.epam.controller.factory.prepare;

import com.epam.model.pizza.component.dough.Dough;
import com.epam.model.pizza.component.sauce.Sauce;
import com.epam.model.pizza.component.topping.Topping;

public interface PizzaPrepareFactory {

  Dough getDough();

  Sauce getSauce();

  Topping getMeat();

  Topping getBacon();

  Topping getOnion();

  Topping getCheese();

  Topping getCorn();

  Topping getMushroom();

  Topping getOlive();

  Topping getPineapple();

  Topping getSpinach();

  Topping getTomato();
}
