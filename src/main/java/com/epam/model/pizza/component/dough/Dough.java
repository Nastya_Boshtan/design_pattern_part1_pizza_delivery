package com.epam.model.pizza.component.dough;

public interface Dough {
    String toString();
    double getPrice();
}
