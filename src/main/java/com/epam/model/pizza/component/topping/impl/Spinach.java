package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Spinach implements Topping {
    public String toString() {
        return "Spinach";
    }

    @Override
    public double getPrice() {
        return 15;
    }
}
