package com.epam.model.pizza.component.sauce.impl;

import com.epam.model.pizza.component.sauce.Sauce;

public class MarinaraSauce implements Sauce {
    public String toString() {
        return "Marinara";
    }

    @Override
    public double getPrice() {
        return 13;
    }
}
