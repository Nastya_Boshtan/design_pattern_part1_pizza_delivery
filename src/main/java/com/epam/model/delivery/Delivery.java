package com.epam.model.delivery;

import com.epam.model.enums.DeliveryType;
import com.epam.model.pizza.Pizza;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Delivery {

  private static Logger log = LogManager.getLogger(Delivery.class);

  protected abstract double deliverPizza(DeliveryType deliveryType, List<Pizza> pizzas, int distance);

  public double sendPizza(DeliveryType deliveryType, List<Pizza> pizzas, int distance) {
    double pizzasToDeliver = deliverPizza(deliveryType, pizzas, distance);
    //todo some actions same for all type deliver services
    return pizzasToDeliver;
  }}
