package com.epam.model.pizza.component.sauce;

public interface Sauce {
    String toString();
    double getPrice();
}
