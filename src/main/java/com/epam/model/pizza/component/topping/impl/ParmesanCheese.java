package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class ParmesanCheese implements Topping {
    public String toString() {
        return "Parmesan cheese";
    }

    @Override
    public double getPrice() {
        return 14;
    }
}
