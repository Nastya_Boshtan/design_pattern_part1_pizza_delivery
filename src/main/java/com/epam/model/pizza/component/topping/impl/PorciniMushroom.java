package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class PorciniMushroom implements Topping {
    public String toString() {
        return "Porcini mushrooms";
    }

    @Override
    public double getPrice() {
        return 18;
    }
}
