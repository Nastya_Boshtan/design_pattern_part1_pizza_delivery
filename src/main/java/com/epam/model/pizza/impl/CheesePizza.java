package com.epam.model.pizza.impl;

import com.epam.controller.factory.prepare.PizzaPrepareFactory;
import com.epam.model.enums.PizzaStatus;
import com.epam.model.pizza.Pizza;
import com.epam.model.enums.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CheesePizza extends Pizza {

  private static Logger log = LogManager.getLogger(CheesePizza.class);
  private PizzaPrepareFactory pizzaPrepareFactory;

  public CheesePizza(PizzaPrepareFactory pizzaPrepareFactory) {
    this.pizzaStatus = PizzaStatus.ORDERED;
    this.pizzaPrepareFactory = pizzaPrepareFactory;
    this.pizzaType = PizzaType.CHEESE;
  }

  public void prepare() {
    log.info("Started preparing pizza " + this.pizzaType);
    this.dough = pizzaPrepareFactory.getDough();
    this.sauce = pizzaPrepareFactory.getSauce();
    this.toppings.add(pizzaPrepareFactory.getCheese());
    this.toppings.add(pizzaPrepareFactory.getCorn());
    this.toppings.add(pizzaPrepareFactory.getTomato());
    this.price =
        pizzaPrepareFactory.getDough().getPrice() + pizzaPrepareFactory.getSauce().getPrice()
            + getPrice();
    log.info(String.format("Pizza %s already prepared", this));
    this.pizzaStatus = PizzaStatus.PREPARED;
  }
}
