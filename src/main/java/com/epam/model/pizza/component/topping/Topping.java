package com.epam.model.pizza.component.topping;

public interface Topping {
    String toString();
    double getPrice();
}
