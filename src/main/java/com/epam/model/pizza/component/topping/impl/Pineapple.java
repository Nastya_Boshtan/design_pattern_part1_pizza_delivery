package com.epam.model.pizza.component.topping.impl;

import com.epam.model.pizza.component.topping.Topping;

public class Pineapple implements Topping {
    public String toString() {
        return "Pineapple";
    }

    @Override
    public double getPrice() {
        return 13;
    }
}
