package com.epam.controller.factory.order;

import com.epam.model.Order;
import com.epam.model.delivery.impl.KyivDelivery;
import com.epam.model.enums.City;
import com.epam.model.enums.DeliveryType;
import com.epam.model.enums.PizzaType;
import com.epam.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class KyivOrder {
    private static Logger log = LogManager.getLogger(KyivOrder.class);
    private static Scanner input = new Scanner(System.in);
    Order kyivOrder;
    KyivDelivery kyivDelivery;

    private void getCheesePizza() {
        kyivOrder.addPizzaToOrder(PizzaType.CHEESE);
    }

    private void getCapricciosa() {
        kyivOrder.addPizzaToOrder(PizzaType.CAPRICCIOSA);
    }

    private void getPepperoniPizza() {
        kyivOrder.addPizzaToOrder(PizzaType.PEPPERONI);
    }

    private void getVeggie() {
        kyivOrder.addPizzaToOrder(PizzaType.VEGGIE);
    }

    public void showKyivOrder() {
        String keyMenu;
        kyivOrder = new Order(City.KYIV);
        kyivDelivery = new KyivDelivery();
        do {
            Menu.getPizzaMenu();
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        getCheesePizza();
                        break;
                    case "2":
                        getCapricciosa();
                        break;
                    case "3":
                        getPepperoniPizza();
                        break;
                    case "4":
                        getVeggie();
                    case "Q":
                        log.info("Approved for delivery status: \n");
                        log.info(kyivOrder.checkPizzaReadyToDeliver());
                        Menu.getDeliveryMenu();
                        kyivOrder.deliverOrderToClient(DeliveryType.UBER, 50);
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
